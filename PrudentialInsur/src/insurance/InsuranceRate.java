package insurance;

public class InsuranceRate {
	private String description;
	private double premiumTermRate;
	private double gstrate;
	private int billingfrequency;
	private int premium_end_Date;
	private int risk_End_Date;
	
	
	public int getPremium_end_Date() {
		System.out.println(premium_end_Date);
		return premium_end_Date;
	}
	public void setPremium_end_Date(int premium_end_Date) {
		this.premium_end_Date = Integer.parseInt(Insurance.getPremium_end_Date().replaceAll("-", ""));
	}
	public int getRisk_End_Date() {
		return risk_End_Date;
	}
	public void setRisk_End_Date(int risk_End_Date) {
		System.out.println(risk_End_Date);
		this.risk_End_Date = Integer.parseInt(Insurance.getRisk_End_Date().replaceAll("-", ""));
	}
	public int getBillingfrequency() {
		return billingfrequency= Integer.valueOf(Insurance.getBilling_Frequency());
	}
	public void setBillingfrequency(int billingfrequency) {
		this.billingfrequency = billingfrequency;
	}
	public double getGstrate() {
		return gstrate;
	}
	public void setGstrate(double gstrate) {
		this.gstrate = gstrate;
	}
	private String production;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPremiumTermRate() {
		return premiumTermRate;
	}
	public void setPremiumTermRate(double premiumTermRate) {
		this.premiumTermRate = premiumTermRate;
	}
	
	public String getProduction() {
		return production;
	}
	public void setProduction(String production) {
		this.production = production;
	}
    
}
