package postgresql;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateTable {
	static Statement stmt = null;

	static StringBuffer createQuery = new StringBuffer(
			"create table SMARTRACKER.TMP_OUTPUT(");

	public static String tableCreation() {
		Connection con = DBUtility.openConnection();
		String stQuery = "";
		try {
			stmt = con.createStatement();
			Field[] listfield = DBUtility.fieldsInResponse();
			for (Field field : listfield) { // Getting all fields one by one
				if (!field.getName().equals("serialVersionUID")) {
					if (field.getType().getSimpleName().equals("String")) {

						createQuery
								.append(field.getName() + " character varying[20], ");

					} 
					
					else if (field.getType().getSimpleName().equals("int")) {

						createQuery.append(field.getName() + " int, ");

					} else if (field.getType().getSimpleName().equals("double")) {

						createQuery
								.append(field.getName() + " decimal(18,0), ");

					} 
					else if (field.getType().getSimpleName().equals("Date")) {

						createQuery.append(field.getName() + " datetime, ");

					}
					else if (field.getType().getSimpleName().equals("boolean")) {
						createQuery.append(field.getName() + " nvarchar(20), ");
				}
			}
			stQuery = createQuery.toString();
			stQuery = stQuery.replaceAll(", $", ")");

			DatabaseMetaData dbMetaData = con.getMetaData();
			ResultSet rs = dbMetaData.getTables(null, "SMARTRACKER","TMP_OUTPUT", null);

			if (rs.next()) {
				System.out.println("Table already exists.");
			} else {
				try {
					int i = stmt.executeUpdate(stQuery);
					System.out.println(i + " table created...");
				} catch (SQLException e) {
					System.out.println("Table may be availale already.");
				}
				//con.commit();

			}

		} 
		}
			catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBUtility.closeConnection();
		}
		return stQuery;
	}

}

