package postgresql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class CreateCheck {
	static final String JDBC_DRIVER = "org.postgresql.Driver"; 
	   static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";

	   //  Database credentials
	   static final String USER = "postgres";
	   static final String PASS = "123";
	   
	   public static void main(String[] args) {
		   System.out.println("Create Check Main DB");
	   Connection conn = null;
	   Statement stmt = null;
	   try{
	      //STEP 2: Register JDBC driver
	      Class.forName("org.postgresql.Driver");

	      //STEP 3: Open a connection
	      System.out.println("Connecting to a selected database...");
	      conn = DriverManager.getConnection(DB_URL, USER, PASS);
	      System.out.println("Connected database successfully...");
	      
	      
	      //STEP 4: Execute a query
	      System.out.println("Creating table in given database...");
	      stmt = conn.createStatement();
	      
	      String sql = "CREATE TABLE SMARTRACKER.TMP_OUTPUT" +
	                   "(CONTRACT_NUMBER VARCHAR(20)," +
	                   "COMPONENT_CODE VARCHAR(10)," + 
	                   "RIDER_NUMBER VARCHAR(2),"+
	                   "COVERAGE_NUMBER VARCHAR(2),"+
	    		       "LIFE_NUMBER VARCHAR(2),"+
	                  "RISK_STATUS VARCHAR(20),"+
	                  "AGENT_NUMBER varchar(20)," +
	    		       "T2_UNIT_NUMBER VARCHAR(10)," + 
	    		       "T3_UNIT_NUMBER VARCHAR(10)," + 
	    		       "CONCATE VARCHAR(30)," +
	    		       "CONCATE_NEW VARCHAR(30))" ;

	      stmt.executeUpdate(sql);
	      System.out.println("Created table in given database...");
	     /* String sql1 = "INSERT INTO SMARTRACKER.TMP_INPUT(CONTRACT_NUMBER,LIFE_NUMBER,COVERAGE_NUMBER,RIDER_NUMBER,RISK_STATUS,PREMIUM_STATUS,ISSURANCE_DATE,COMPONENT_CODE,PRODUCT_CODE,PREMIUM_END_DATE,INSTALLMENT_PREMIUM,RISK_END_DATE,BILLING_FREQUENCY,BATCH_TRANSACTION_CODE,LIFE_CLIENT_NUM,AGENT_NUMBER,T2_UNIT_NUMBER,T3_UNIT_NUMBER,CURRENCY,CHANNEL)"
	      		+ "VALUES ('66791061','01','01','00','IF','PP','180701','XAD8','PAU','20251122',50.00,'20001122',12.0,'T642','12345678','13329','B14','K73','SGD','AG')";
	      stmt.executeUpdate(sql1);
	      System.out.println("Inserted values into table in given database...");*/
	   }catch(SQLException se){
	      //Handle errors for JDBC
	      se.printStackTrace();
	   }catch(Exception e){
	      //Handle errors for Class.forName
	      e.printStackTrace();
	   }finally{
	      //finally block used to close resources
	      try{
	         if(stmt!=null)
	            conn.close();
	      }catch(SQLException se){
	      }// do nothing
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }//end finally try
	   }//end try
	   System.out.println("Goodbye!");
	}//end main

}
