package postgresql;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import insurance.Premium;
import insurance.Sample;
public class DBUtility {
	static Connection con = null;

	public static Connection openConnection() {
		try {
			Class.forName(DBConstant.DB_DRIVER);
			con = DriverManager.getConnection(DBConstant.DB_URL, DBConstant.DB_USER, DBConstant.DB_PASSWORD);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		return con;
	}

	public static void closeConnection() {
		try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	public static Field[] fieldsInResponse(){
		Field[] field = Sample.class.getDeclaredFields();
		return field;
	}
	
	public static String responseQuery(){
		
		Field[] field = DBUtility.fieldsInResponse();
		List<String> listField = new ArrayList<>();
		// This is the partial implement Insert Query. Need to put ? (index) inside VALUES().
		StringBuffer RESPONSE_INSERT_QUERY = new StringBuffer("INSERT INTO SMARTRACKER.TMP_OUTPUT VALUES(");
		
		// Add all the required variables into the List<String>
		for(Field f : field){
			if(!f.getName().equals("serialVersionUID")){
								
					listField.add(f.getName());
				
			}
		}		
		// Here, the code is for putting ? (index) dynamically based on how many columns you want to insert.
		for( int i = 0 ; i < listField.size(); i++ ) {
			RESPONSE_INSERT_QUERY =  i < listField.size() - 1 ? RESPONSE_INSERT_QUERY.append("?,") : RESPONSE_INSERT_QUERY.append("?");
		}
		
		RESPONSE_INSERT_QUERY.append(")");		// Finally, close the brace for VALUES after putting all ? (index)
		return RESPONSE_INSERT_QUERY.toString();
	}
	
}
