package postgresql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import postgresql.DBConstant;
import insurance.Insurance;

public class RequestDAO {
	
	

	static Statement stmt = null;
	static ResultSet resultSet = null;
	public static List<Insurance> getCheckList() {
		Connection con = DBUtility.openConnection();
		String query=DBConstant.CHECK_QUERY; 

		List<Insurance> checkList = new ArrayList<>();
		try {
			stmt = con.createStatement();

			resultSet = stmt.executeQuery(query);

			if (resultSet != null) {
				while (resultSet.next()) {
					Insurance check = new Insurance();
						/*check.setAge(resultSet.getInt(DBConstant.AGE));
					if (resultSet.getString(DBConstant.NAME) != null)
						check.setName(resultSet.getString(DBConstant.NAME));
					if (resultSet.getString(DBConstant.QUALIFICATION) != null)
						check.setName(resultSet.getString(DBConstant.QUALIFICATION));*/
					check.setBilling_Frequency(resultSet.getString("BILLING_FREQUENCY"));
					check.setChannel(resultSet.getString("CHANNEL"));
					check.setComponent_Code(resultSet.getString("COMPONENT_CODE"));
					check.setCoverage_Number(resultSet.getString("COVERAGE_NUMBER"));
					check.setAgent_Number(resultSet.getString("AGENT_NUMBER"));
					check.setBatch_transaction_Code(resultSet.getString("BATCH_TRANSACTION_CODE"));
					check.setContract_Number(resultSet.getString("CONTRACT_NUMBER"));
					check.setCurrency(resultSet.getString("CURRENCY"));
					check.setInstallment_Premium(resultSet.getDouble("INSTALLMENT_PREMIUM"));
					check.setIssurance_Date(resultSet.getString("ISSURANCE_DATE"));
					check.setLife_Client_Number(resultSet.getString("LIFE_CLIENT_NUM"));
					check.setLife_Number(resultSet.getString("LIFE_NUMBER"));
					check.setPremium_end_Date(resultSet.getString("PREMIUM_END_DATE"));//
					check.setPremium_Status(resultSet.getString("PREMIUM_STATUS"));
					check.setProduct_Code(resultSet.getString("PRODUCT_CODE"));
					check.setRider_Number(resultSet.getString("RIDER_NUMBER"));
					check.setRisk_End_Date(resultSet.getString("RISK_END_DATE"));  
					check.setRisk_Status(resultSet.getString("RISK_STATUS"));
					check.setT2_Unit_Number(resultSet.getString("T2_UNIT_NUMBER"));
					check.setT3_Unit_Number(resultSet.getString("T3_UNIT_NUMBER"));
					
					
					

					System.out.println(
	                         check.getAgent_Number() + "\t" 
	                         + check.getBatch_transaction_Code() + "\t" 
	                         + check.getChannel()+ "\t"
	                         + check.getComponent_Code() + "\t"
	                         + check.getContract_Number() + "\t"
	                         + check.getCoverage_Number() + "\t"
	                         + check.getCurrency() + "\t"
	                         + check.getInstallment_Premium() + "\t"
	                         +check.getLife_Client_Number() + "\t"
	                         +check.getT2_Unit_Number() + "\t"
	                         + check.getLife_Number() + "\t"
	                         + check.getPremium_Status() + "\t"
	                         + check.getProduct_Code()+ "\t"
	                         + check.getRider_Number() + "\t"
	                         + check.getRisk_Status() + "\t"
	                         + "Issurance Date " + check.getIssurance_Date() + "\t"
	                         + check.getT3_Unit_Number() + "\t"
	                         + Insurance.getPremium_end_Date() + "\t"
	                         + Insurance.getRisk_End_Date() + "\t"
	                         
	                         
	             
	                         );
					
					checkList.add(check);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			resultSet = null;
		}
		return checkList;
	}

}
